var webpack = require('webpack');
var fs = require('fs');
var path = require('path');
var yargs = require('yargs');
var HtmlWebpackPlugin = require('html-webpack-plugin');

// Here you must add each option/flag you need. Without it even webpack specific flags will throw an exception.
var options = {
    env: yargs.argv.e || 'dev'
};

var localHelper = {
    isProd(){
        return options.env === 'prod';
    }
};

var paths = {
    base: {
        src:    path.resolve(__dirname, './src'),
        vendor: path.resolve(__dirname, './node_modules'),
        dist:   path.resolve(__dirname, './dist')
    }
};

// common config
var baseConfig = {
    cache: true,
    watch: true,
    devtool: 'eval'
};

var baseEntryConfig = {
    context: paths.base.src
};

// loaders
var loaders = {
    babel: {
        test: /\.js$/,
        // loaders: ['react-hot', 'babel'],
        loaders: ['babel'],
        include: path.join(__dirname, 'src')
    },
    html:        {
        test:    /\.html$/,
        exclude: /node_modules/,
        loader:  'html'
    },
    json: {
        test: /\.json$/,
        exclude: /node_modules/,
        loader: 'json'
    },
    sass:        {
        test:    /\.sass$/,
        exclude: /node_modules/,
        loader:  'style!css!sass'
    },
    images:      {
        dev:  {
            test:    /\.(jpe?g|png|gif|svg)$/i,
            loaders: [
                'file?hash=sha512&digest=hex&name=image.[hash].[ext]',
                'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
            ]
        },
        // Using pngquant
        prod: {
            test:    /.*\.(gif|png|jpe?g|svg)$/i,
            loaders: [
                'file?hash=sha512&digest=hex&name=image.[hash].[ext]',
                'image-webpack?{progressive:true, optimizationLevel: 7, interlaced: false, pngquant:{quality: "65-90", speed: 4}}'
            ]
        }
    },
    fonts:       [
        {
            test:   /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/font-woff&prefix=fonts&name=font.[hash].[ext]'
        }, {
            test:   /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/octet-stream&prefix=fonts&name=font.[hash].[ext]'
        }, {
            test:   /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=application/vnd.ms-fontobject&prefix=fonts&name=font.[hash].[ext]'
        }, {
            test:   /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url?limit=10000&mimetype=image/svg+xml&prefix=fonts&name=font.[hash].[ext]'
        }
    ]
};

// plugins
var plugins = {
    commonChunks: () => {
        return new webpack.optimize.CommonsChunkPlugin({
            name:      'vendors',
            filename:  'vendors.[hash].js',
            minChunks: (module, count) => {
                return module.resource && module.resource.indexOf('node_modules') !== -1 && count >= 1;
            }
        });
    },
    html: (options) => {
        return new HtmlWebpackPlugin(options);
    },
    provide: () => {
        return new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            React: 'react',
            _: 'lodash',
            moment: 'moment'
        });
    },
    define: () => {
        return new webpack.DefinePlugin({
            __DEV__: options.env === 'dev',
            __TEST__: options.env === 'test',
            __PROD__: options.env === 'prod'
        });
    },
    uglifyJs: () => {
        return new webpack.optimize.UglifyJsPlugin({
            mangle:   false,
            compress: {
                warnings: false
            }
        });
    },
    dedupe: () => {
        return new webpack.optimize.DedupePlugin();
    },
    sourceMap: (path) => {
        return new webpack.SourceMapDevToolPlugin({
            filename: '[file].map',
            append:   `\n//# sourceMappingURL=${path}[url]`
        })
    },
    hmr: () => {
        return new webpack.HotModuleReplacementPlugin();
    }
};

// resolves
var resolve = {
    modulesDirectories: ['node_modules']
};

// config data per entry
var entryPoints = {
    entry: [
        './app'
    ],
    resolve: resolve,
    module:  {
        loaders: [
            loaders.babel,
            loaders.json,
            loaders.sass,
            localHelper.isProd() ? loaders.images.prod : loaders.images.dev,
            loaders.fonts
        ]
    },
    output:  {
        path: paths.base.dist,
        publicPath: '/',
        filename: 'app.[chunkhash].js',
        chunkFilename:'[name].[chunkhash].js'
    },
    plugins: [
        plugins.commonChunks(),
        plugins.define(),
        plugins.provide(),
        plugins.sourceMap('/'),
        plugins.html({
            template: path.join(paths.base.src, 'index.html'),
            inject:   'body',
            filename: 'index.html',
            favicon:  'favicon.ico'
        }),
        plugins.hmr()
    ]
};

if (localHelper.isProd()) {
    entryPoints.plugins = entryPoints.plugins.concat([
        plugins.uglifyJs(),
        plugins.dedupe()
    ]);
}

module.exports = Object.assign({}, baseEntryConfig, entryPoints, baseConfig);

require('babel-register');

var Express = require('express');
var del = require('del');
var webpack = require('webpack');
var webpackConfig = require('./webpack.config');
var yargs = require('yargs');
var fs = require('fs');
var path = require('path');
var merge = require('merge');
var WriteFilePlugin = require('write-file-webpack-plugin');
var webpackDevMiddleware = require("webpack-dev-middleware");
var webpackHotMiddleware = require("webpack-hot-middleware");
var options = {
  env: getEnv()
};

cleanDist();
generateConfig();

webpackConfig.plugins = webpackConfig.plugins.concat([
  // new webpack.HotModuleReplacementPlugin(),
  new WriteFilePlugin()
]);
webpackConfig.entry = webpackConfig.entry.concat([
  'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
]);

var compiler = webpack(webpackConfig);
var app = new Express();

app.use(webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  quiet: false,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  historyApiFallback: true,
  cache: true,
  colors: true,
  host: '127.0.0.1',
  stats: {
    colors: true,
    errors: true,
    warnings: true,
    chunks: false,
    version: false,
    assets: false,
    timings: false,
    entrypoints: false,
    chunkModules: false,
    chunkOrigins: false,
    cached: false,
    cachedAssets: false,
    reasons: false,
    usedExports: false,
    children: false,
    source: false,
    modules: false
  }
}));

app.use(webpackHotMiddleware(compiler, {
  log: console.log,
  path: '/__webpack_hmr',
  heartbeat: 10 * 1000,
}));

app.get('*', function(req, res) {
  /*eslint-disable */
  var index = fs.readFileSync('./dist/index.html');
  /*eslint-enable */
  res.end(index);
}.bind(this));

app.listen(8000, 'localhost', function (err, result) {
  if (err) {
    return console.log(err);
  }

  console.log('Listening at http://localhost:8000/');
});

/**
 *
 */
function generateConfig() {
  const baseConfig = require('./config/base.json');
  const currentConfig = require(`./config/${options.env}.json`);

  fs.writeFileSync('./config.json', JSON.stringify(merge(baseConfig, currentConfig), null, '\t'));
}


/**
 *
 * @returns {*}
 */
function getEnv() {
  if (yargs.argv.p) {
    return 'prod';
  }
  if (yargs.argv.e) {
    return yargs.argv.e;
  }

  return 'dev';
}

function cleanDist() {
    del.sync('./dist', {force: true});
}
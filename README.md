# Table of Contents

* [Intro](#markdown-header-intro)
* [Technologies stack](#markdown-header-technologies-stack)
* [Prerequisites](#markdown-header-prerequisites)
* [Start](#markdown-header-start)
* [Under the Hood](#markdown-header-under-the-hood)

## Intro
This is the solution for [1aim room booking challenge](https://challenges.1aim.com/roombooking/).
  
## Technologies stack
* building: [webpack](http://webpack.github.io) with [hot module replacement](http://webpack.github.io/docs/hot-module-replacement.html), and [babel](http://babeljs.io);
* core: [React](https://facebook.github.io/react) and [Redux](https://github.com/reactjs/redux)
* validation: [validate.js](http://validatejs.org/)
* configuration fetching: [selectn](https://www.npmjs.com/package/selectn);
* styling with [bootstrap-sass-webpack](https://github.com/twbs/bootstrap-sass)
* logging with [browser-bunyan](https://www.npmjs.com/package/browser-bunyan)
* browser detection with [bowser](https://www.npmjs.com/package/bowser)
* range sliding with [jQRangeSlider](http://ghusse.github.io/jQRangeSlider/)

## Prerequisites
* Node.js v.5*
* global installation of [napa](https://www.npmjs.com/package/napa): `npm i -g napa` (requires **sudo** for linux distributives) 

## Start
Simply run `npm install` for dependencies installation and then `npm start` in order to run application.
The web-server will be available on http://localhost:8000.

## Under the Hood
1. start script runs **./server.js** file where [Webpack dev server](http://webpack.github.io/docs/webpack-dev-server.html) with [webpack dev middleware](http://webpack.github.io/docs/webpack-dev-middleware.html) are used with simpliest [express.js](http://expressjs.com/) backend app for serve development process;
2. **./server.js** file uses webpack & webpack dev server configurations from corresponding files of **./webpack** directory;
3. After webpack finishes its work express app initializes middlewares for webpack as such (watching, serving ) and hot module replacement;
4. due to the settings of [Babel](http://babeljs.io/) in **./.babelrc** file after each change of source files webpack uses its [Babel loader](https://github.com/babel/babel-loader) with [Babel React plugins](http://babeljs.io/docs/plugins/preset-react/) in order to update the browser without page refresh.

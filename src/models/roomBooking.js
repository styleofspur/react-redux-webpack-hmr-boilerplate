import BaseModel from '../base/models/base';
import {HttpError} from '../errors/hub';

export default class RoomBookingModel extends BaseModel {
    
    _url = '';
    
    /**
     * 
     * @param data
     * @param options
     * @returns {Promise}
     */
    send(data = this.attributes, options = {}) {
        return super.create(`${this._url}/sendpass`, data, options).then(response => {
            if (response.error) {
                throw new HttpError(response.error);
            }
        });
    }
    
}
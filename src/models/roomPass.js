import BaseModel from '../base/models/base';

export default class RoomModel extends BaseModel {
    
    _url = '';

    /**
     *
     * @param data
     * @returns {Promise}
     */
    validate(data = this.attributes) {
        const messages = {
            emailRequired:   BaseModel.getValidationMsg('Email is required'),
            emailInvalid:    BaseModel.getValidationMsg('Email is invalid'),
            nameRequired:    BaseModel.getValidationMsg('Name is required'),
            numberRequired:   BaseModel.getValidationMsg('Phone number is required')
        };

        const rules = {
            email: {
                presence: { message: () => messages.emailRequired },
                email:    { message: () => messages.emailInvalid }
            },
            name: {
                presence: { message: () => messages.nameRequired }
            },
            number: {
                presence: { message: () => messages.numberRequired }
            }
        };

        return super.validate(data, { rules });
    }
    
}
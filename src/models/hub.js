import RoomPass    from './roomPass';
import RoomBooking from './roomBooking';

export const RoomPassModel    = RoomPass;
export const RoomBookingModel = RoomBooking;
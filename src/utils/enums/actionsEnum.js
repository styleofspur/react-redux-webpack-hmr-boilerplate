/**
 * Represents a list of application actions
 */
export default class ActionsEnum {

    static NOTIFICATION = {
        OPEN: 'NOTIFICATION_OPEN',
        CLOSE: 'NOTIFICATION_CLOSE'
    };

    static ROOMS = {
        FETCHING: 'ROOMS_FETCHING',
        FETCHED: 'ROOMS_FETCHED',
        FETCH_ERROR: 'ROOMS_FETCH_ERROR',
        SHOW_DETAILS: 'ROOMS_SHOW_DETAILS',
        
        FILTER: 'ROOMS_FILTER',
        TOGGLE_DETAILED_VIEW: 'ROOMS_TOGGLE_DETAILED_VIEW',

        BOOKING: {
            TIME: {
                SUCCESS: 'ROOMS_BOOKING_TIME_SUCCESS',
                ERROR:   'ROOMS_BOOKING_TIME_ERROR'
            },
            FORM: {
                SUCCESS: 'ROOMS_BOOKING_FORM_SUCCESS',
                ERROR:   'ROOMS_BOOKING_FORM_ERROR'
            },
            SUCCESS: 'ROOMS_BOOKING_SUCCESS',
            ALREADY_BOOKED: 'ROOMS_BOOKING_ALREADY_BOOKED',
            ERROR: 'ROOMS_BOOKING_ERROR'
        }
    };
}
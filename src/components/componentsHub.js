import DatesPagerControl          from './datesPager';
import NotificationPopupComponent from './notificationPopup';

export const DatesPager        = DatesPagerControl;
export const NotificationPopup = NotificationPopupComponent;
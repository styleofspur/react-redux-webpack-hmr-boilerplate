import {browserHistory} from 'react-router';
import {GeneralHelper} from '../../helpers/hub';
import './style.sass';

/**
 *  Button control component.
 *
 *  If you want to stylize this control use next selectors:
 *  @selector .button
 *      For the only element
 *
 */
export default class Button extends React.Component {

    static propTypes = {
        text: React.PropTypes.string,
        title: React.PropTypes.string,
        type: React.PropTypes.string,
        cssClass: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        onClick: React.PropTypes.func,
        navigationHash: React.PropTypes.string
    };
    
    static defaultProps = {
        text: '',
        type: 'button',
        title: '',
        cssClass: '',
        disabled: false
    };
    
    static elementClass = 'button';

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        if (this.props.navigationHash) {
            browserHistory.push(this.props.navigationHash);
        } else if (GeneralHelper.isFunction(this.props.onClick)) {
            this.props.onClick(e);
        }
    }

    render() {
        let options = {
            className: Button.elementClass,
            type: this.props.type,
            onClick: this.onClick
        };

        if (this.props.disabled) {
            options['disabled'] = 'disabled';
        }
        if (this.props.cssClass) {
            options['className'] = `${Button.elementClass} ${this.props.cssClass}`
        }
        if (this.props.title) {
            options['title'] = this.props.title;
        }

        return (
            <button {...options} >
                {this.props.text || this.props.children}
            </button>
        );
    }
}

import BaseComponent from '../../base/components/component';
import {ConfigHelper, GeneralHelper} from '../../helpers/hub';
import './style.sass';

export default class DatesPager extends BaseComponent {

    static propTypes = {
        format:       React.PropTypes.string,
        currentDate:  React.PropTypes.number,
        prevDateStep: React.PropTypes.number,
        nextDateStep: React.PropTypes.number,
        onClick:      React.PropTypes.func
    };

    static defaultProps = {
        format:       ConfigHelper.get('dateFormat'),
        currentDate:  Date.now(),
        prevDateStep: 1,
        nextDateStep: 1
    };

    static elementClass = 'dates-pager';

    constructor() {
        super(...arguments);

        this.onNext = this.onNext.bind(this);
        this.onPrev = this.onPrev.bind(this);
    }

    onNext() {
        if (GeneralHelper.isFunction(this.props.onClick)) {
            this.props.onClick(
                moment(this.props.currentDate).add(this.props.nextDateStep, 'day')
            );
        }
    }

    onPrev() {
        if (GeneralHelper.isFunction(this.props.onClick)) {
            this.props.onClick(
                moment(this.props.currentDate).subtract(this.props.prevDateStep, 'day')
            );
        }
    }

    render() {

        const diff = moment(this.props.currentDate).subtract(this.props.prevDateStep, 'day').diff(moment(), 'days');

        return (
            <div className={DatesPager.elementClass}>
                <ul className="pager">
                    <li className="" onClick={this.onPrev}>
                        {
                            // @todo: find the better solution for such situation
                            diff > 0 || diff === -0  ?
                                <a href="#">
                                    <i className="fa fa-chevron-left fa-3" aria-hidden="true"/>
                                </a> : <span/>
                        }
                    </li>
                    <li className="current-date">
                        <span>
                            {
                                moment(this.props.currentDate).format(this.props.format)
                            }
                        </span>
                    </li>
                    <li className="" onClick={this.onNext}>
                        <a href="#">
                            <i className="fa fa-chevron-right fa-3" aria-hidden="true"/>
                        </a>
                    </li>
                </ul>
            </div>
        );
    }

}
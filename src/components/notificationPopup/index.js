import {connect} from 'react-redux'
import BaseContainer from '../../base/containers/container';
import {AppHelper, GeneralHelper} from '../../helpers/hub';
import './style.sass';

export class NotificationPopup extends BaseContainer {

    static propTypes = {
        items: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
    };

    static elementClass = 'notification-popup';

    constructor(props) {
        super(...arguments);

        if (!GeneralHelper.isArray(props.items)) {
            throw new Error('NotificationPopup container: prop "items" is required and must be an array.');
        }
        
        this.close = this.close.bind(this);
    }

    close(){
        AppHelper.hideNotification();
    };
    
    render() {
        const popupHideClass = this.props.items.length == 0 ? 'hidden' : '';
        return (
            <div className={`${NotificationPopup.elementClass} ${popupHideClass}`} ref="popup">
                <i id={`${NotificationPopup.elementClass}-close`} className="fa fa-times" onClick={this.close}></i>
                <div className="container-fluid">
                    <div className={`${NotificationPopup.elementClass}-content`}>
                        <ul id={`${NotificationPopup.elementClass}-validationErrors`}>
                            {
                                this.props.items.map(function (itemToShow, i) {
                                    return (<li key={i}>{itemToShow}</li>)
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: GeneralHelper.isUndefined(state.notification) ? [] : state.notification.items
    };
};

export default connect(mapStateToProps)(NotificationPopup);
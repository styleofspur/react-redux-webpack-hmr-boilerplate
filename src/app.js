import './polyfill';

import ReactDOM                        from 'react-dom';
import {Router, match, browserHistory} from 'react-router';
import {syncHistoryWithStore}          from 'react-router-redux';
import {Provider}                      from 'react-redux';
import createStore                     from './store';

import routes from './routes';
import {
    LocalStorageHelper,
    StoreHelper,
    ConfigHelper,
    LogHelper
} from './helpers/hub';
import './style.sass';

const location = window.location.pathname;
const rootEl   = document.getElementById('app');

const renderApp = (store, history, key) => {
    const routes = require('./routes');
    ReactDOM.render(
        <Provider store={store}>
            <Router routes={routes} history={history} key={key} />
        </Provider>,
        rootEl
    );
};

const hmr = () => {
    if (__DEV__ && module.hot) {
        module.hot.accept(['./app', './routes'], () => {
            //history = syncHistoryWithStore(browserHistory, store);
            renderApp(store, history, Math.random());
        });
    }
};

let store = createStore();
let history = syncHistoryWithStore(browserHistory, store);

Promise.resolve().then(() => {
    return ConfigHelper.init();
}).then(() => {
    return LocalStorageHelper.init();
}).then(() => {
    return StoreHelper.init(store);
}).then(() => {
    return LogHelper.init();
}).then(() => {
    let history = syncHistoryWithStore(browserHistory, store);

    match({routes, location}, () => renderApp(store, history, Math.random()));
    hmr();

}).catch(err => {
    LogHelper.error(err);
});
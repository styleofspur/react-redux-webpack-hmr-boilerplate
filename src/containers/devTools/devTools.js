import {EnvHelper} from '../../helpers/hub';

export default () => {
    return  EnvHelper.isDev() && window && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f;
};

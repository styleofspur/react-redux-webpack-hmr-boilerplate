import {connect}       from 'react-redux';
import FormPage        from '../../base/pages/formPage';
import {RoomsActions}  from '../../actions/hub';
import {
    RoomPassModel,
    RoomBookingModel
} from '../../models/hub';
import {
    AppHelper,
    StoreHelper
} from '../../helpers/hub';

export class RoomBookingForm extends FormPage {

    constructor(props) {
        super(...props);

        this.model    = new RoomPassModel();
        this.onSubmit = this.onSubmit.bind(this);
    }


    /**
     *
     * @param event
     */
    onSubmit(event) {
        Object.assign(this.model.attributes, FormPage.getFormData(event.target));

        super.processValidationFormSubmit(event, this.model)
            .then(() => {
                const roomBooking = new RoomBookingModel({
                    booking: StoreHelper.getState().rooms.booking,
                    passes: [
                        this.model.attributes
                    ]
                });
                
                roomBooking.send()
                    .then(() => {
                        AppHelper.showNotification(['The room was successfully booked!']);
                        StoreHelper.dispatch(RoomsActions.form.success(this.model.attributes));
                    })
                    .catch(errors => {
                        AppHelper.showNotification(super.prepareServerErrorsForNotification(errors));
                        StoreHelper.dispatch(RoomsActions.form.error(errors));
                    });
            }).catch(errors => {
                console.log(errors);
                StoreHelper.dispatch(RoomsActions.form.error(errors));
            });
    };

    render() {
        return (

            <div 
                className="modal fade" 
                id="bookForm" 
                tabIndex="-1"
                role="dialog" 
                aria-labelledby="myModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">

                        <div className="modal-header">
                            <button type="button" className="close"
                                    data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span className="sr-only">Close</span>
                            </button>
                            <h4 className="modal-title" id="myModalLabel">
                                Room Booking
                            </h4>
                        </div>
                        <div className="modal-body">
                            <form
                                className="form-horizontal"
                                role="form"
                                noValidate="novalidate"
                                autoComplete="off"
                                onSubmit={this.onSubmit}
                            >
                                <div className="form-group">
                                    <label
                                        className="col-sm-2 control-label"
                                        htmlFor="nameInput"
                                    >
                                        Name
                                    </label>
                                    <div className="col-sm-10">
                                        <input
                                            type="text"
                                            name="name"
                                            className="form-control"
                                            id="nameInput"
                                            placeholder="Name"
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label
                                        className="col-sm-2 control-label"
                                        htmlFor="emailInput"
                                    >
                                        Email
                                    </label>
                                    <div className="col-sm-10">
                                        <input
                                            type="text"
                                            name="email"
                                            className="form-control"
                                            id="emailInput"
                                            placeholder="Email"
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label
                                        className="col-sm-2 control-label"
                                        htmlFor="numberInput"
                                    >
                                        Phone
                                    </label>
                                    <div className="col-sm-10">
                                        <input
                                            type="text"
                                            name="number"
                                            className="form-control"
                                            id="numberInput"
                                            placeholder="Phone (XXX) XXX-XX-XX"
                                        />
                                    </div>
                                </div>
                                <div className="form-group" style={{textAlign: 'center'}}>
                                    <button type="submit" className="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}

export default connect()(RoomBookingForm);
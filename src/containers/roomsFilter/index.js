import {connect} from 'react-redux';
import Switch from 'react-toggle-switch';
import BaseComponent from '../../base/components/component';
import {RoomsActions} from '../../actions/hub';
import {StoreHelper} from '../../helpers/hub';

import './style.sass';

export class RoomsFilter extends BaseComponent {

    static elementClass = 'rooms-filter';

    constructor() {
        super(...arguments);

        this.toggleAvailabilityNow = this.toggleAvailabilityNow.bind(this);
        this.filterByName          = this.filterByName.bind(this);
    }

    /**
     *
     */
    toggleAvailabilityNow() {
        StoreHelper.dispatch(RoomsActions.filter({
            availableNow: !this.props.filter.availableNow
        }));
    }

    /**
     * 
     */
    filterByName(e) {
        StoreHelper.dispatch(RoomsActions.filter({
            name: e.target.value
        }));
    }
    
    render() {

        return (
            <div className={`${RoomsFilter.elementClass} form-inline`}>
                <div className="form-group name-filter">
                    <label htmlFor="nameFilter">Filter</label>
                    &nbsp;
                    <input
                        type="text"
                        name="name"
                        id="nameFilter"
                        placeholder="Room Name"
                        className="form-control"
                        onChange={this.filterByName}
                        value={this.props.filter.name || ''}
                    />
                </div>
                <div className="form-group available-now-filter">
                    <label
                        htmlFor="availableNowFilter"
                        onClick={this.toggleAvailabilityNow}
                    >
                        available now
                    </label>
                    <Switch
                        on={this.props.filter.availableNow}
                        type="checkbox"
                        id="availableNowFilter"
                        onClick={this.toggleAvailabilityNow}
                    />
                </div>
                <div className="clearfix"></div>
            </div>
        );
    }
};

const mapStateToProps = state => {
    
    return {
        filter: state.rooms.filter
    };
};

export default connect(mapStateToProps)(RoomsFilter);

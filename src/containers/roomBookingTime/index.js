import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/mouse';
import 'jqRangeSlider/jQRangeSliderMouseTouch';
import 'jqRangeSlider/jQRangeSliderDraggable';
import 'jqRangeSlider/jQRangeSliderBar';
import 'jqRangeSlider/jQRangeSliderHandle';
import 'jqRangeSlider/jQRangeSliderLabel';
import 'jqRangeSlider/jQRangeSlider';
import 'jqRangeSlider/jQDateRangeSliderHandle';
import 'jqRangeSlider/jQDateRangeSlider';
import 'jqRangeSlider/jQRuler';

import {connect}      from 'react-redux';
import BaseContainer  from '../../base/containers/container';
import BaseComponent  from '../../base/components/component';
import {RoomsActions} from '../../actions/hub';
import {
    ConfigHelper,
    GeneralHelper,
    StoreHelper,
    AppHelper
} from '../../helpers/hub';

import './style.sass';

export class RoomBookingTime extends BaseContainer {

    static elementClass = 'range-slider';
    
    static propTypes = {
        name:  React.PropTypes.string,
        avail: React.PropTypes.array
    };
    
    constructor() {
        super(...arguments);

        this.onChange            = this.onChange.bind(this);
        this.getCurrentRouteDate = this.getCurrentRouteDate.bind(this);
        this.id                  = BaseComponent.getUniqueId(RoomBookingTime.elementClass);
    }

    getCurrentRouteDate() {
        const routeDateParam = StoreHelper.getState().routing.locationBeforeTransitions.pathname.substr(1);
        const parts          = routeDateParam.split('.');
        let date             = null;           
        
        if (parts.length === 3) {
            const dateObj = new Date();
            dateObj.setDate(+parts[0]);
            dateObj.setMonth(+parts[1] - 1);
            dateObj.setYear(+parts[2]);

            date = dateObj;
        }
        
        return date;
    }

    /**
     * 
     * @param e
     * @param data
     */
    onChange(e, data) {
        const startMinutes = data.values.min.getHours() * 60 + data.values.min.getMinutes();
        const endMinutes   = data.values.max.getHours() * 60 + data.values.max.getMinutes() + ConfigHelper.get('rooms.minutesBetweenBookings');

        if (GeneralHelper.isAvailable(startMinutes, endMinutes, this.props.avail)) {
            const currentDate = this.getCurrentRouteDate() || new Date();
            const startDate   = new Date(
                Date.UTC(
                    currentDate.getFullYear(),
                    currentDate.getMonth(),
                    currentDate.getDate(),
                    data.values.min.getHours(),
                    data.values.min.getMinutes()
                )
            );
            const endDate    = new Date(
                Date.UTC(
                    currentDate.getFullYear(),
                    currentDate.getMonth(),
                    currentDate.getDate(),
                    data.values.max.getHours(),
                    data.values.max.getMinutes()
                )
            );

            const booking = {
                date:        currentDate.getTime(),
                time_start:  startDate.getTime(),
                time_end:    endDate.getTime(),
                room:        this.props.name,
                title:       'Test Title',
                description: 'Test Description'
            };

            StoreHelper.dispatch(RoomsActions.time.success(booking));
        } else {
            StoreHelper.dispatch(RoomsActions.time.error());
            AppHelper.showNotification(['Selected time range overlaps with already booked one.']);
        }
    }

    isAvailable(start, end) {
        const startTimeInMinutes = start.getHours() * 60 + start.getMinutes();
        const endTimeInMinutes   = end.getHours() * 60 + end.getMinutes();

        return GeneralHelper.isAvailable(
            startTimeInMinutes,
            endTimeInMinutes,
            this.props.avail
        );
    }
    
    componentDidMount() {
        const minTime    = ConfigHelper.get('rooms.minTime');
        const minHours   = +minTime.split(':')[0];
        const minMinutes = +minTime.split(':')[1] || 0;

        const maxTime    = ConfigHelper.get('rooms.maxTime');
        const maxHours   = +maxTime.split(':')[0];
        const maxMinutes = +maxTime.split(':')[1] || 0;

        const addZero = (val) => {
            if (val < 10) {
                return "0" + val;
            }

            return val;
        };

        // Possible use, if sliders are constructed/destroyed many times
        setTimeout(() => {
            $(document).on(`valuesChanged`, `#${this.id}`, this.onChange);
        }, 1500);

        $(`#${this.id}`).dateRangeSlider({
            arrows: false,
            bounds: {
                min: new Date(
                    new Date().getFullYear(),
                    new Date().getMonth(),
                    new Date().getDate(),
                    minHours,
                    minMinutes
                ),
                max: new Date(
                    new Date().getFullYear(),
                    new Date().getMonth(),
                    new Date().getDate(),
                    maxHours,
                    maxMinutes
                )
            },
            range: {
                min: {
                    minutes: ConfigHelper.get('rooms.minBookingTime')
                }
            },
            step: {
                minutes: ConfigHelper.get('rooms.step')
            },
            valueLabels: 'change',
            durationIn:  1000,
            durationOut: 1000,
            scales: [{
                first: function (value) {
                    return value;
                },
                end: function (value) {
                    return value;
                },
                next: function (value) {
                    var next = new Date(value);
                    return new Date(next.setHours(value.getHours() + 1));

                },
                label: function (value) {
                    return value.getHours();
                }
            }, {
                first: function (value) {
                    return value;
                },
                end: function (value) {
                    return value;
                },
                next: function (value) {
                    var next = new Date(value);
                    return new Date(next.setMinutes(value.getMinutes() + 15));

                },
                label: function (value) {
                    return '';
                },
                format: (tickContainer, tickStart, tickEnd) => {
                    tickContainer.addClass(this.isAvailable(tickStart, tickEnd) ? 'available' : 'booked');
                }
            }],
            formatter: function (val) {
                var h = val.getHours(),
                    m = val.getMinutes();

                return addZero(h) + ':' + addZero(m);
            }
        });


    }


    render() {

        return (
            <div className={RoomBookingTime.elementClass}>
                <div id={this.id}></div>
            </div>
        );

    }
}

export default connect()(RoomBookingTime);
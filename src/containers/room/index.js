import {connect}                from 'react-redux';
import BaseComponent            from '../../base/components/component';
import {RoomBookingTime}        from '../containersHub';
import {RoomsActions}           from '../../actions/hub';
import {
    StoreHelper,
    DomHelper,
    ConfigHelper
} from '../../helpers/hub';
import './style.sass';

export class Room extends BaseComponent {

    static elementClass = 'room';

    static propTypes = {
        room:             React.PropTypes.object,
        detailedViewId:   React.PropTypes.string,
        detailedViewFlag: React.PropTypes.bool
    };

    constructor(props) {
        super(...arguments);

        window.addEventListener('resize', () => {
            if (DomHelper.isOutOfSupportedWidth()) {
                console.log('render message');
            }
        });

        this.id             = `room-${props.room.name}`;
        this.toggleRoomView = this.toggleRoomView.bind(this);
    }

    /**
     *
     */
    toggleRoomView() {
        StoreHelper.dispatch(
            RoomsActions.toggleDetailedView(
                this.props.room.name,
                this.props.room.name === this.props.detailedViewId ?
                    !this.props.detailedViewFlag : true
            )
        );
        // window.location.hash = `#${this.id}`;
    }
    
    render() {

        return (
            <div className={`${Room.elementClass} panel panel-default`} id={this.id}>
                <div className={`${Room.elementClass}-header panel-heading`}>
                    <div className="row">
                        <div className="col-sm-6 col-xs-6 room-name">
                            Room: <span>{this.props.room.name}</span>
                        </div>
                        <div className="col-sm-6 col-xs-6 book-button">
                            <button className="btn btn-primary btn-md" data-toggle="modal" data-target="#bookForm">
                                Book
                            </button>
                        </div>
                    </div>
                    <RoomBookingTime
                        name={this.props.room.name}
                        avail={this.props.room.avail} 
                    />
                </div>
                <div className={`${Room.elementClass}-details panel-content`}>
                    <div className="row">
                        <div className="col-sm-6 col-xs-6">
                            <span className="field-name col-sm-4 col-xs-6">
                                <img src={require('../../assets/icons/location.png')} />
                            </span>
                            <span className="field-value col-sm-4 col-xs-6">
                                {this.props.room.location}
                            </span>
                        </div>
                        <div className="col-sm-6 col-xs-6">
                            <span className="field-name col-sm-4 col-xs-6">
                                <img src={require('../../assets/icons/size.png')} />
                            </span>
                            <span className="field-value col-sm-4 col-xs-6">
                                {this.props.room.size}
                            </span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-xs-6">
                            <span className="field-name col-sm-4 col-xs-6">
                                <img src={require('../../assets/icons/capacity.png')} />
                            </span>
                            <span className="field-value col-sm-4 col-xs-6">
                                {this.props.room.capacity}
                            </span>
                        </div>
                        <div className="col-sm-6 col-xs-6">
                            <a href="#" className="col-sm-4" onClick={this.toggleRoomView}>
                                {
                                    this.props.detailedViewId === this.props.room.name && this.props.detailedViewFlag ?
                                        'Hide Details' : 'Details'
                                }
                            </a>
                        </div>
                    </div>
                    {
                        this.props.detailedViewId === this.props.room.name && this.props.detailedViewFlag ?
                            <DetailedRoomView room={this.props.room} /> : <div/>
                    }
                </div>

            </div>
        );
    }
}

/**
 *
 * @param props
 * @returns {XML}
 * @constructor
 */
export const DetailedRoomView = props => {

    return (
        <div>
            <div className="row">
                <div className="col-sm-6">
                    <p className="field-name col-sm-4 col-xs-6">
                        <img src={require('../../assets/icons/equipment.png')} />
                    </p>
                    <div className="col-sm-6">
                    {
                        props.room.equipment.map((item, i) => {
                            return <div key={i}>{item}</div>
                        })
                    }
                    </div>
                </div>
            </div>
            <div className="row">
            {
                props.room.images.map((imageUrl, i) => {
                    return (
                        <div key={i} className="room-image col-sm-4">
                            <img
                                width="100%"
                                src={`${ConfigHelper.get('api.url')}/${imageUrl}`}
                            />
                        </div>
                    );
                })
            }
            </div>
        </div>
    );

};

const mapStateToProps = (state, props) => {

    return Object.assign(...props, {
        detailedViewId:   state.rooms.detailedView.name,
        detailedViewFlag: state.rooms.detailedView.show
    });

};

export default connect(mapStateToProps)(Room);
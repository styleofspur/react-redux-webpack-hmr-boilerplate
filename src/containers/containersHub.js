import RoomsFilterContainer     from './roomsFilter';
import RoomContainer            from './room';
import RoomBookingTimeContainer from './roomBookingTime';
import RoomBookingFormContainer from './roomBookingForm';

export const Room            = RoomContainer;
export const RoomsFilter     = RoomsFilterContainer;
export const RoomBookingTime = RoomBookingTimeContainer;
export const RoomBookingForm = RoomBookingFormContainer;
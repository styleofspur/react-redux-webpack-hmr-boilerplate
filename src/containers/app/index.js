import {NotificationPopup} from '../../components/componentsHub';
import './style.sass';

export default class App extends React.Component {

    static contextTypes = {
        store: React.PropTypes.object,
        router: React.PropTypes.object
    };

    render() {
        return (
            <div className="app-container">
                <div className="container-fluid page-container">
                    {this.props.children}
                </div>
                <NotificationPopup />
            </div>
        );
    }
};


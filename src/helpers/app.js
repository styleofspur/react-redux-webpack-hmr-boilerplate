import {StoreHelper} from './hub';
import {NotificationActions} from '../actions/hub';

export default class AppHelper {

    static _hideNotificationTimeout = 5000;

    /**
     * Shows notification in the bottom of the page.
     * 
     * @param items
     * @param hideAfterTimeout
     */
    static showNotification(items = [], hideAfterTimeout = true){
        StoreHelper.dispatch(NotificationActions.open(items));

        if (hideAfterTimeout) {
            setTimeout(AppHelper.hideNotification, AppHelper._hideNotificationTimeout);
        }
    }

    /**
     * Hides any type of bottom notifications.
     */
    static hideNotification(){
        StoreHelper.dispatch(NotificationActions.close());
    }
}

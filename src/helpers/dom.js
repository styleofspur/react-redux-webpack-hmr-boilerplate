import isMobile from 'ismobilejs/isMobile.min.js';
import {GeneralHelper, ConfigHelper} from './hub';

export default class DomHelper {

    static isMobileScreen(){
        let size = DomHelper.getViewportSize();
        return size.width <= ConfigHelper.get('dom.mobileWidth');
    }

    static isOutOfSupportedWidth() {
        return DomHelper.getViewportSize().width < ConfigHelper.get('dom.minWidth');
    }
    
    /**
     * Returns flag that indicates whether the user agent detected as mobile or not.
     * @returns {boolean}
     */
    static isMobileDevice(){
        return isMobile.any;
    }

    /**
     * Gets viewable area of the user's device.
     * @returns {*}
     */
    static getViewportSize() {
        let size;

        if (!GeneralHelper.isUndefined(window.innerWidth)) {
            size = {
                width: window.innerWidth,
                height: window.innerHeight
            };
        } else if (!GeneralHelper.isUndefined(document.documentElement) && !GeneralHelper.isUndefined(document.documentElement.clientWidth) && document.documentElement.clientWidth != 0) {
            size = {
                width: document.documentElement.clientWidth,
                height: document.documentElement.clientHeight
            };
        } else{
            let body = document.getElementsByTagName('body')[0];
            size = {
                width: body.clientWidth,
                height: body.clientHeight
            };
        }
        return size;
    }

}

import {ConfigHelper} from './hub';
import {HttpError} from '../errors/hub';
import {HttpMethodsEnum} from '../utils/enums/hub';

export default class RequestHelper {

    // @todo: use package for requests mocks
    static _settings = {
        mock: {
            enabled: false
        }
    };

    static _options = {
        dataType: 'JSON',
        timeout: 10000
    };

    /**
     *
     * @param url
     * @returns {string}
     * @private
     */
    static _prepareUrl(url) {
        return `${ConfigHelper.get('api.url')}/${url}`;
    }

    /**
     * Sends request
     *
     * @param method
     * @param url
     * @param data
     * @param options
     * @returns {Promise}
     * @private
     */
    static _send(method, url, data, options) {
        url = RequestHelper._prepareUrl(url);
        data = Object.keys(data).length ? JSON.stringify(data) : '';
        
        return RequestHelper._settings.mock.enabled ?
            RequestHelper._sendMockRequest(method, url) :
            RequestHelper._sendRealRequest(method, url, data, options);
    }

    /**
     *
     * @param method
     * @param url
     * @param data
     * @param options
     * @returns {Promise}
     * @private
     */
    static _sendRealRequest(method, url, data, options = {}) {
        return new Promise((resolve, reject) => {
            $.ajax({
                    method,
                    url,
                    data,
                    ...RequestHelper._options,
                    ...options
                })
                .done(data => {
                    resolve(data);
                })
                .error(jqXHR => {
                    reject(new HttpError(jqXHR));
                });
        });
    }

    /**
     *
     * @returns {Promise}
     * @private
     */
    static _sendMockRequest() {
        return new Promise((resolve, reject) => {
            const dataToReturn = RequestHelper._settings.mock.response.data;

            RequestHelper._settings.mock.response.error ?
                reject(dataToReturn) :
                resolve(dataToReturn);

            RequestHelper._settings.mock.response = {}; // reset mock response
        });
    }

    /**
     *
     * @param options
     */
    static startMock(options = {}) {
        RequestHelper._settings.mock = {
            enabled: true
        };

        if (Object.keys(options)) {
            Object.assign(RequestHelper._settings.mock, options);
        }
    }

    /**
     *
     * @param data
     * @param error
     */
    static setMockResponse(data = {}, error = false) {
        RequestHelper._settings.mock.response = {
            data,
            error
        };
    }

    /**
     *
     */
    static endMock() {
        RequestHelper._settings.mock = {
            enabled: false
        };
    }

    /**
     * Sends GET request
     *
     * @param url
     * @param data
     * @param options
     * @returns {*}
     */
    static get(url = '', data = {}, options = {}) {
        return RequestHelper._send(HttpMethodsEnum.GET, url, data, options)
            .then(
                resp => Promise.resolve(resp),
                error => {
                    throw {
                        message: error.message,
                        status: error.status
                    };
                }
            );
    }

    /**
     * Sends POST request
     *
     * @param url
     * @param data
     * @param options
     * @returns {*}
     */
    static post(url = '', data = {}, options = {}) {
        return RequestHelper._send(HttpMethodsEnum.POST, url, data, options)
            .then(
                resp => Promise.resolve(resp),
                error => {
                    throw {
                        message: error.message,
                        status: error.status
                    };
                }
            );
    }

    /**
     * Sends PUT request
     *
     * @param url
     * @param data
     * @param options
     * @returns {*}
     */
    static put(url = '', data = {}, options = {}) {
        return RequestHelper._send(HttpMethodsEnum.PUT, url, data, options)
            .then(
                resp => Promise.resolve(resp),
                error => {
                    throw {
                        message: error.message,
                        status: error.status
                    };
                }
            );
    }

    /**
     * Sends DELETE request
     *
     * @param url
     * @param options
     * @returns {*}
     */
    static delete(url = '', options = {}) {
        return RequestHelper._send(HttpMethodsEnum.DELETE, url, options)
            .then(
                resp => Promise.resolve(resp),
                error => {
                    throw {
                        message: error.message,
                        status: error.status
                    };
                }
            );
    }

}

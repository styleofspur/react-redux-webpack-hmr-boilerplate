/* global require */
import selectn from 'selectn';

export default class ConfigHelper{
    
    static get(path) {
        return selectn(path, ConfigHelper);
    }

    static init() {
        return new Promise((resolve, reject) => {
            require('bundle?lazy&name=home!../../config.json')(config => {
                for(let prop in config) {
                    if (config.hasOwnProperty(prop)) {
                        ConfigHelper[prop] = config[prop];
                    }
                }
                resolve();
            }, reject);
        });
    }
};
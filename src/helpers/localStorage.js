import {ConfigHelper, GeneralHelper} from './hub';

export default class LocalStorageHelper {

    static keys = {
        reduxStore: 'reduxStore',
        user: 'user'
    };
    
    static _localStorage = null;
    static _prefix = null;

    static init(storage) {

        return new Promise((resolve, reject)=> {
            LocalStorageHelper._prefix = ConfigHelper.get('localStorage.prefix');
            LocalStorageHelper._localStorage = storage || (typeof window !== 'undefined' ? window.localStorage : null);

            resolve();
        });
    }

    /**
     *
     * @param key
     */
    static get(key) {
        let prefixedKey = LocalStorageHelper.generateKey(key);

        if (!LocalStorageHelper._hasItem(prefixedKey)) {
            throw new Error(`LocalStorageHelper doesn't have item [${key}]`);
        }

        let item = LocalStorageHelper._localStorage.getItem(prefixedKey);
        if (!item) {
            return null;
        }

        try {
            return JSON.parse(item).value;
        } catch (e) {
            throw new Error(`LocalStorageHelper has invalid value of [${key}]`);
        }

    }

    /**
     *
     * @param key
     */
    static remove(key) {
        return LocalStorageHelper._localStorage.removeItem( LocalStorageHelper.generateKey(key) );
    }

    /**
     *
     * @param {string} key
     * @param {*} value
     * @param {object} options
     */
    static set(key, value, options = {}) {

        LocalStorageHelper._validate(...arguments);

        LocalStorageHelper._localStorage.setItem( LocalStorageHelper.generateKey(key), JSON.stringify({
            value,
            options
        }));
    }

    /**
     *
     * @param prefixedKey
     * @returns {boolean}
     */
    static _hasItem(prefixedKey) {
        return !!LocalStorageHelper._localStorage.getItem(prefixedKey);
    }

    /**
     * Gets the flag that indicates whether key is already in the storage or not.
     * @param key
     * @returns {boolean}
     */
    static containsKey(key) {
        let prefixedKey = LocalStorageHelper.generateKey(key);
        return LocalStorageHelper._hasItem(prefixedKey);
    }

    /**
     *
     * @private
     * @param {string} key
     * @param {*} value
     * @param {object} options
     */
    static _validate(key, value, options = {}) {

        if (!GeneralHelper.isString(key)) {
            throw new Error(`LocalStorageHelper key must be a string`);
        }

        if (GeneralHelper.isUndefined(value)) {
            throw new Error(`LocalStorageHelper value must be something`);
        }

        if (options.expiration && !(options.expiration instanceof Date)) {
            throw new Error(`LocalStorageHelper [${key}] expiration option must be a Date object`);
        }
    }

    /**
     *
     * @param key
     * @returns {*}
     */
    static generateKey(key) {
        return `${LocalStorageHelper._prefix}${key}`;
    }
    
}

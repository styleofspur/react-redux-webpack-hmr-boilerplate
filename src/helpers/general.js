import sha1 from 'sha1';
import {LogHelper} from './hub'
import {BaseError} from '../errors/hub';

export default class GeneralHelper {

    /**
     * You need to provide this method array of functions that RETURN PROMISE and not array of promises
     * @param arr
     * @returns {Promise}
     */
    static runPromiseSequence(arr){
        if(!arr) return;

        return new Promise((resolve, reject) => {
            arr.reduce((curr, next)=>{
                return curr.then(next);
            }, Promise.resolve()).then(resolve).catch(reject);
        });
    }

    /**
     * Converts the string to boolean or returns null if the object is undefined.
     * @param value The value to convert.
     */
    static convertStringToBool(value){
        if(!value) return null;

        try{
            return JSON.parse(value.toLowerCase());
        }catch(err){
            LogHelper.warning('Couldn\'t convert string to boolean.', { inputValue : value });
            throw new BaseError('Couldn\'t convert string to boolean.');
        }
    }

    /**
     * Converts the string to number or returns null if the object is undefined.
     * @param value The value to convert.
     */
    static convertStringToNumber(value){
        if(!value) return null;

        try{
            return JSON.parse(value);
        }catch(err){
            LogHelper.warning('Couldn\'t convert string to number.', { inputValue : value });
            throw new BaseError('Couldn\'t convert string to number.');
        }
    }

    /**
     * Converts the string to object or returns null if the object is undefined.
     * @param value The value to convert.
     */
    static convertStringToObject(value){
        if(!value) return null;

        try{
            return JSON.parse(decodeURIComponent(value));
        }catch(err){
            LogHelper.warning('Couldn\'t convert string to number.', { inputValue : value });
            throw new BaseError('Couldn\'t convert string to number.');
        }
    }

    /**
     * Returns a flag that indicates whether the input val is string or not.
     * @param val The value to check.
     * @returns {boolean}
     */
    static isString(val){
        return typeof val === 'string';
    }

    /**
     * Returns a flag that indicates whether the input val is number or not.
     * @param val The value to check.
     * @returns {boolean}
     */
    static isNumber(val){
        return typeof val === 'number';
    }

    /**
     * Returns a flag that indicates whether the input val is undefined or not.
     * @param val The value to check.
     * @returns {boolean}
     */
    static isUndefined(val){
        return typeof val === 'undefined';
    }
    
    /**
     * Returns a flag that indicates whether the input val is object or not.
     * @param val The value to check.
     * @returns {boolean}
     */
    static isObject(val){
        return typeof val === 'object';
    }

    /**
     * Returns a flag that indicates whether the input val is array or not.
     * @param val The value to check.
     * @returns {boolean}
     */
    static isArray(val){
        return Array.isArray ? Array.isArray(val) : val instanceof Array;
    }

    /**
     * Returns a flag that indicates whether the input val is function or not.
     * @param val The value to check.
     * @returns {boolean}
     */
    static isFunction(val){
        return typeof val === 'function';
    }

    /**
     * Loads an image wrapped onload event in promise.
     * @param url
     * @returns {Promise}
     * @constructor
     */
    static loadImage(url) {
        return new Promise((resolve, reject)=> {
            var tmpImg = new Image();
            tmpImg.src = url;
            tmpImg.onload = () => resolve(tmpImg);
        });
    }

    /**
     *
     * @param start
     * @param end
     * @param intervals
     * @returns {boolean}
     */
    static isAvailable(start, end, intervals) {
        let isAvailable = false;

        intervals.some(availInterval => {
            const timeParts    = availInterval.split(' - ');

            const startParts   = timeParts[0].split(':');
            const startHours   = +startParts[0];
            const startMinutes = +startParts[1] || 0;

            const endParts   = timeParts[1].split(':');
            const endHours   = +endParts[0];
            const endMinutes = +endParts[1] || 0;

            const startAvailIntervalInMinutes = startHours * 60 + startMinutes;
            const endAvailIntervalInMinutes   = endHours * 60 + endMinutes;

            if (start >= startAvailIntervalInMinutes && end <= endAvailIntervalInMinutes) {
                isAvailable = true;
                return false;
            }

        });

        return isAvailable;
    }

    /**
     * Generates the key from http request.
     * @param method
     * @param model
     * @param options
     */
    static getHTTPRequestCacheKey(method, model, options = {}){
        let body = '';
        if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
            body = JSON.stringify(options.attrs || model.toJSON(options));
        }

        return sha1(options.url + body);
    }
}
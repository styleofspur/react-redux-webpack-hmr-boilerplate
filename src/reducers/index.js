import { routerReducer as routing } from 'react-router-redux'
import notification from './notification';
import rooms from './rooms';

export {
    notification,
    rooms,
    routing
};
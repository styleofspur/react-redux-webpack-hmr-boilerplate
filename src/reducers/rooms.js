import {ActionsEnum} from '../utils/enums/hub';

const filterByName = () => {

};

const initialState = {
    fetchError:     null,
    list:           [],
    filter:         {},
    booking:        null,
    detailedView:   {
        name: '',
        show: false
    }
};

const rooms = (state, action) => {

    switch (action.type) {
        case ActionsEnum.ROOMS.FETCHING:
            return state;
        case ActionsEnum.ROOMS.FETCHED:
            return Object.assign({}, state, {
                list: action.rooms.sort((room1, room2) => {
                    const [room1int, room1float] = room1.name.split('.');
                    const [room2int, room2float] = room2.name.split('.');

                    if (+room1int === +room2int) {
                         return +room1float > +room2float;
                    } else {
                         return +room1int > +room2int;
                    }
                })
            });
        case ActionsEnum.ROOMS.FETCH_ERROR:
            return Object.assign({}, state, {
                fetchError: action.error
            });

        case ActionsEnum.ROOMS.FILTER:
            return Object.assign({}, state, {
                filter: Object.assign({}, state.filter, action.filter)
            });

        case ActionsEnum.ROOMS.BOOKING.TIME.SUCCESS:
            return Object.assign({}, state, {
                booking: action.booking
            });
        
        case ActionsEnum.ROOMS.TOGGLE_DETAILED_VIEW:
            return Object.assign({}, state, {
                detailedView: {
                    name: action.name,
                    show: action.show
                }
            });

        default:
            return state || initialState;
    }
};

export default rooms;
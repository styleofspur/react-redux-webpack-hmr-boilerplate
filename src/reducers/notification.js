import {ActionsEnum} from '../utils/enums/hub';

const initialState = {
    items: []
};

const notification = (state = initialState, action) => {

    switch (action.type) {
        case ActionsEnum.NOTIFICATION.OPEN:
            return {items: action.items};
        case ActionsEnum.NOTIFICATION.CLOSE:
            return {items:[]};
        default:
            return state;
    }
};

export default notification;
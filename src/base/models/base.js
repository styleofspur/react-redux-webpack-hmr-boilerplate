import validate from 'validate.js';
import DataRequester from '../dataRequester';
import {GeneralHelper} from '../../helpers/hub';

export default class BaseModel extends DataRequester {

    static passwordRegex = /^(?=.*[A-z])(?=.*[0-9])(?!.*[\s]).{8,}$/;
    
    constructor(attributes) {
        super();
        this.attributes = {};
        
        if (GeneralHelper.isObject(attributes)) {
            this._merge(attributes);
        }
    }

    /**
     *
     * @param name
     * @param value
     */
    set(name, value) {
        this.attributes[name] = value;
    }

    /**
     *
     * @param name
     * @returns {*}
     */
    get(name) {
        return this.attributes[name];
    }

    /**
     *
     * @param data
     * @private
     */
    _merge(data = {}) {
        if (GeneralHelper.isObject(data)) {
            for (const prop in data) {
                if (data.hasOwnProperty(prop)) {
                    this.attributes[prop] = data[prop];
                }
            }
        }
    }

    /**
     * Validates data by the rules of defined validation strategy.
     * @param data
     * @param settings
     * @returns {a|*}
     */
    validate(data, settings){
        if(settings.validators){
            for(let validatorName in settings.validators){
                validate.validators[validatorName] = settings.validators[validatorName];
            }
        }
        return validate(data, settings.rules);
    }

    /**
     * 
     * @param term
     * @returns {string}
     */
    static getValidationMsg(term){
        return `^${term}`;
    }

}
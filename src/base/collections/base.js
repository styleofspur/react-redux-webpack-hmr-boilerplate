import DataRequester from '../dataRequester';
import {GeneralHelper} from '../../helpers/hub';

export default class BaseCollection extends DataRequester {

    constructor(items = []) {
        super();
        this.items = [];
        
        if (GeneralHelper.isArray(items) && items.length) {
            this._merge(items);
        }
    }

    /**
     *
     * @param item
     */
    add(item) {
        this.items.push(item);
    }

    /**
     *
     * @param id
     */
    remove(id) {
        let i = -1;

        this.items.some((item, key) => {
            if (item.id === id) {
                i = key;
                return false;
            }
        });

        if (i > -1) {
            this.items.splice(i, 1);
        }
    }

    /**
     *
     * @param id
     * @returns {*}
     */
    get(id) {
        let foundItem = null;

        this.items.some(item => {
            if (item.id === id) {
                foundItem = item;
                return false;
            }

        });

        return foundItem;
    }

    /**
     *
     * @param data
     * @private
     */
    _merge(data = []) {
        if (GeneralHelper.isArray(data)) {
            this.items = data;
        }
    }
    

}
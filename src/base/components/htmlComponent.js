import BaseComponent from './component'

export default class HtmlComponent extends BaseComponent {
    static propTypes = {
        id: React.PropTypes.string.isRequired,
        required: React.PropTypes.bool,
        value: React.PropTypes.any,
        placeholder: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        onChange: React.PropTypes.func
    };

    static defaultProps = {
        placeholder: '',
        required: true,
        disabled: false
    };

    static hideClass = 'hide';

    /**
     * Gets asterisk icon classes.
     * @param prefix
     * @returns {string}
     */
    getAsteriskClasses(prefix){
        let asteriskClasses = [`${prefix}-asterisk`, 'glyphicon', 'glyphicon-asterisk'];

        if(!this.props.required){
            asteriskClasses.push(HtmlComponent.hideClass);
        }

        return asteriskClasses.join(' ');
    }


    /**
     * Gets element classes.
     * @param prefix
     * @returns {string}
     */
    getElementClasses(prefix){
        let rootElClasses = [ prefix ];
        if (this.props.required)
            rootElClasses.push('required');
        if (this.props.disabled)
            rootElClasses.push('disabled');

        return rootElClasses.join(' ');
    }
};

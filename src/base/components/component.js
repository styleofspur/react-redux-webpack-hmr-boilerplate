export default class BaseComponent extends React.Component {
    /**
     * Provides with unique ID bases on component styles prefix and current date.
     * @param prefix
     * @returns {string}
     */
    static getUniqueId(prefix) {
        return `${prefix}_${Math.random().toString(36).substr(2, 16)}`;
    }
};
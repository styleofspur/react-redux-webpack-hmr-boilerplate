import {GeneralHelper, RequestHelper, LocalStorageHelper} from '../helpers/hub';
import {HttpMethodsEnum} from '../utils/enums/hub';

export default class DataRequester {
    
    /**
     *
     * @param url
     * @param data
     * @param options
     * @returns {*|V}
     */
    fetch(url = '', data = {}, options = {}) {
        return new Promise((resolve, reject) => {

            const method = options.method || HttpMethodsEnum.GET;
            const key = GeneralHelper.getHTTPRequestCacheKey(
                method, this.items || this.attributes,
                Object.assign({}, options, {url: this._url})
            );

            if (options.cache && LocalStorageHelper.containsKey(key)) {
                let cachedData = LocalStorageHelper.get(key);
                this._merge(cachedData.data);
                resolve();
            } else {

                RequestHelper[method](url, data, options).then(data => {
                    this._merge(data);
                    if (options.cache) {
                        LocalStorageHelper.set(key, {
                            data: this.items || this.attributes
                        })
                    }
                    resolve();
                }, reject);

            }
        });
    }

    /**
     *
     * @param url
     * @param data
     * @param options
     * @returns {Promise}
     */
    create(url = '', data = {}, options = {}) {
        return new Promise((resolve, reject) => {
            RequestHelper[HttpMethodsEnum.POST](url, data, options).then(data => {
                this._merge(data);
                resolve();
            }, reject);
        });
    }

    /**
     *
     * @param url
     * @param data
     * @param options
     * @returns {Promise}
     */
    update(url = '', data = {}, options = {}) {
        return new Promise((resolve, reject) => {
            RequestHelper[HttpMethodsEnum.PUT](url, data, options).then(data => {
                this._merge(data);
                resolve();
            }, reject);
        });
    }

    /**
     *
     * @param url
     * @param options
     * @returns {Promise}
     */
    remove(url = '', options = {}) {
        return new Promise((resolve, reject) => {
            RequestHelper[HttpMethodsEnum.DELETE](url, options).then(data => {
                this._merge(data || {});
                resolve();
            }, reject);
        });
    }
}
export default class BasePage extends React.Component {

    /**
     *
     * @param err
     */
    prepareServerErrorsForNotification(err = {}) {
        return [err.message];
    }
};
import BasePage from './page';
import {AppHelper} from '../../helpers/hub';

export default class FormPage extends BasePage {

    static getFormData(form) {
        // // cross-platform way to get all form's inputs
        form = form.querySelectorAll('input');
        
        let fields = {};
        for (let i = 0; i < form.length; i++) {
            let name = form[i].name;
            if(name) {
                switch (form[i].tagName) {
                    case 'BUTTON':
                        continue;
                        break;
                    default:
                        fields[name] = form[i].value;
                }
            }
        }

        return fields;
    }

    /**
     * Shows validation messages.
     * @param errors
     * @private
     */
    showValidation(errors) {
        AppHelper.showNotification(this.prepareClientErrorsForNotification(errors));
    }

    /**
     * Process form submit with validating data by specific strategy.
     * @param e
     * @param model
     * @returns {Promise}
     */
    processValidationFormSubmit(e = null, model){
        if (e) {
            e.preventDefault();
        }

        return new Promise((resolve, reject)=> {
            let errors = model.validate();
            if (errors) {
                this.showValidation(errors);
                reject(errors);
            } else {
                resolve();
            }
        });
    }

    /**
     *
     * @param err
     * @returns {Array}
     */
    prepareClientErrorsForNotification(err = {}) {
        let arr = [];

        for (let prop in err) {
            arr.push(err[prop][0]);
        }

        return arr;
    }
};
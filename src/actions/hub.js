import * as Notification from './notification';
import * as Rooms from './rooms';

export const NotificationActions = Notification;
export const RoomsActions = Rooms;
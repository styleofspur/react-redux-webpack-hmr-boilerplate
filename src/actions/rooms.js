import {ActionsEnum} from '../utils/enums/hub';
import {AppHelper, StoreHelper} from '../helpers/hub';
import {RoomsCollection} from '../collections/hub';

const fetch = (date = 'now') => {
    const roomsCollection = new RoomsCollection();
    StoreHelper.dispatch(roomsActions.fetching());

    return roomsCollection.fetch(date)
        .then(() => {
            StoreHelper.dispatch(roomsActions.fetched(roomsCollection.items));
        })
        .catch(error => {
            StoreHelper.dispatch(roomsActions.fetchError(error));
            AppHelper.showNotification([error.message]); 
        });
};

const roomsActions = {

    time: {
        success: (booking) => {
            return {
                type: ActionsEnum.ROOMS.BOOKING.TIME.SUCCESS,
                booking
            };
        },

        error: () => {
            return {
                type: ActionsEnum.ROOMS.BOOKING.TIME.ERROR
            }
        } 
    },
    
    form: {
        success: () => {
            return {
                type: ActionsEnum.ROOMS.BOOKING.FORM.SUCCESS
            };
        },

        error: (errors) => {
            return {
                type: ActionsEnum.ROOMS.BOOKING.FORM.ERROR,
                errors
            }
        } 
    },
    
    filter: (filter) => {
        return {
            type: ActionsEnum.ROOMS.FILTER,
            filter
        };
    },
    
    toggleDetailedView: (name, show) => {
        return {
            type: ActionsEnum.ROOMS.TOGGLE_DETAILED_VIEW,
            name,
            show
        };
    },
    
    fetching: () => {
        return {
            type: ActionsEnum.ROOMS.FETCHING
        };
    },
    
    fetched: (rooms) => {
        return {
            type: ActionsEnum.ROOMS.FETCHED,
            rooms
        };
    },
    
    fetchError: (fetchError) => {
        return {
            type: ActionsEnum.ROOMS.FETCH_ERROR,
            fetchError
        };
    }

};

export default Object.assign(roomsActions, { fetch });
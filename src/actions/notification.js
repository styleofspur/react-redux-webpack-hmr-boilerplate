import {ActionsEnum} from '../utils/enums/hub';

export const open = (items) => {
    return {
        type: ActionsEnum.NOTIFICATION.OPEN,
        items
    };
};

export const close = () => {
    return {
        type: ActionsEnum.NOTIFICATION.CLOSE
    };
};
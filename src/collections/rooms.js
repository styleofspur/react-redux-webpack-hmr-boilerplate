import BaseCollection from '../base/collections/base';

export default class RoomsCollection extends BaseCollection {
    
    _url = '';

    fetch(date = 'now', options = {}) {
        return super.create(`${this._url}getrooms`, {date}, options);
    }
    
}
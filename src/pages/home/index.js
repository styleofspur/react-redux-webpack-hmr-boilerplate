import 'bootstrap-sass/assets/javascripts/bootstrap/modal';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import BaseComponent from '../../base/components/component';
import {
    RoomsFilter,
    Room,
    RoomBookingForm
} from '../../containers/containersHub';
import {DatesPager} from '../../components/componentsHub';
import {ConfigHelper, GeneralHelper} from '../../helpers/hub';
import {RoomsActions} from '../../actions/hub';
import './style.sass';

class Home extends BaseComponent {

    static elementClass = 'home';

    static propTypes = {
        list:        React.PropTypes.array,
        currentDate: React.PropTypes.number
    };

    static defaultProps = {
        currentDate: Date.now()
    };

    constructor() {
        super(...arguments);

        this.datePickerFormat = ConfigHelper.get('formats.datePicker');
        this.onDateChange = this.onDateChange.bind(this);
    }

    onDateChange(date) {
        RoomsActions.fetch(new Date(date).getTime());
        browserHistory.push(moment(date).format(ConfigHelper.get('formats.dateUrlParam')));
    }

    componentDidMount() {
        RoomsActions.fetch();
    }

    render() {

        return (
            <div className={`${Home.elementClass}`}>
                <DatesPager
                    format={this.datePickerFormat}
                    currentDate={this.props.currentDate}
                    onClick={this.onDateChange}
                />
                <RoomsFilter/>
                <RoomsList rooms={this.props.rooms}/>
                <RoomBookingForm />
            </div>
        );
    }
}

const RoomsList = props => {

    return (
        <div className="list">
            {
                props.rooms.length ?
                    props.rooms.map((room, i) => {
                        return <Room room={room} key={i}/>
                    }) :
                    <p className="not-found">No rooms found</p>
            }
        </div>
    );
};

const getVisibleRooms = (state) => {

    const rooms = state.rooms.list;
    const filter = state.rooms.filter;

    if (filter.name) {
        return rooms.filter(room => room.name.includes(filter.name));
    }

    if (filter.availableNow) {
        return rooms.filter(room => {
            const nowDate          = new Date();
            const nowTimeInMinutes = nowDate.getHours() * 60 + nowDate.getMinutes();

            return GeneralHelper.isAvailable(
                nowTimeInMinutes,
                nowTimeInMinutes,
                room.avail
            );
        });
    }

    return rooms;

};

const mapStateToProps = (state, props) => {

    let currentDate;
    if (props.params.date) {
        const [day, month, year] = props.params.date.split('.');
        currentDate = (new Date(year, month - 1, day)).getTime();
    } else {
        currentDate = Date.now();
    }

    return {
        rooms: getVisibleRooms(state),
               currentDate
    };
};

export default connect(mapStateToProps)(Home);